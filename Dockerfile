FROM alpine:3.8

MAINTAINER sjaak.meulen@gmail.com

RUN apk add --update cppcheck=1.83-r0
