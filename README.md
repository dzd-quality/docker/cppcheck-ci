CppCheck CI
===========

CppCheck CI is a dockerfile for performing static code analysis on C/C++ projects. The image may be used for any native code, including Arduino projects.

Usage
-----
Add a step to your ```.gitlab-ci.yml``` file to perform static code analysis and use this Dockerfile as your image. Setup the scripts to execute the following command against the ```src``` directory:

```
scripts:
 - cppcheck src/
```
